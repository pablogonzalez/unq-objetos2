import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import unq.Punto;
import unq.Rectangulo;

class RectanguloTestCase {

	private Rectangulo rectangulo;
	
	@BeforeEach
	void setUp() throws Exception {
		rectangulo = new Rectangulo(new Punto(4, 6), 10, 5);
	}

	@Test
	void ComportamientoEsHorizontal() {
		assertEquals(rectangulo.esHorizontal(), true);
	}

	void ComportamientoEsVertical() {
		assertEquals(rectangulo.esVertical(), false);
		assertEquals(rectangulo.getArea(), 50);
		assertEquals(rectangulo.getPerimetro(), 30);
		

	}
}
