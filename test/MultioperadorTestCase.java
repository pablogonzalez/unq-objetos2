import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.sun.tools.javac.util.Assert;

import unq.Multioperador;

class MultioperadorTestCase {

	private Multioperador multioperador; 
	
	@BeforeEach
	void setUp()  throws Exception {
		
		this.multioperador = new Multioperador();
		this.multioperador.addNumero(10);
		this.multioperador.addNumero(5);
		this.multioperador.addNumero(2);
		this.multioperador.addNumero(1);
	}
	
	@Test
	void testSumar() {
		Integer valor = this.multioperador.sumar();
		assertEquals(valor, 18);
	}
	@Test
	void testRestar() {
		Integer valor = this.multioperador.restar();
		assertEquals(valor, 2);
	}
	@Test
	void testMultiplicar() {
		Integer valor = this.multioperador.multiplicar();
		assertEquals(valor, 100);
	}

}
