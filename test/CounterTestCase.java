import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.sun.tools.javac.util.Assert;

import unq.Counter;

public class CounterTestCase {

	private Counter counter;
	
	/**
	 * Crea un escenario de test basico, que consiste en un 
	 * contador con 10 enteros
	 */
	@BeforeEach
	public void setUp() throws Exception{
		//se crea el contador
		counter = new Counter();
		//Se agregan los numeros. Un solo par y nueve impares
		counter.addNumero(1);
		counter.addNumero(3);
		counter.addNumero(5);
		counter.addNumero(7);
		counter.addNumero(9);
		counter.addNumero(1);
		counter.addNumero(1);
		counter.addNumero(1);
		counter.addNumero(1);
		counter.addNumero(4);
	}
	
	@Test
	public void testNumerosImpares() {
		//Getting the even occurrences
		int amount = counter.getCantidadImpares();	
		//Check the amount is the expected one
		assertEquals(amount, 9);
	}
	@Test
	public void testNumerosPares() {
		//Getting the even occurrences
		int amount = counter.getCantidadPares();	
		//Check the amount is the expected one
		assertEquals(amount, 1);
	}
	@Test
	public void testNumerosMultiplosDe() {
		//Getting the even occurrences
		int amount = counter.getCantidadMultiplosDe(3);	
		//Check the amount is the expected one
		assertEquals(amount, 2);
	}
	

}
