package unq;

import java.util.ArrayList;

public class Counter {

	private ArrayList<Integer> numeros;
	
	public Counter() {
		this.numeros = new ArrayList<Integer>();
	}
	
	public void addNumero(Integer numero) {
		this.numeros.add(numero);
	}
	
	public Integer getCantidadPares() {
		
		numeros.forEach((n) -> System.out.println(n)); 
		
		return (int) numeros.stream().filter ( numero -> Counter.esPar(numero) ).count();
	}

	public Integer getCantidadImpares() {
		return (int) numeros.stream().filter ( numero -> !Counter.esPar(numero) ).count();
	}
	
	public Integer getCantidadMultiplosDe(Integer cantidad) {
		return (int) numeros.stream().filter ( numero -> Counter.esMultiploDe(numero, cantidad) ).count();
	}
	
	
	public static Boolean esPar(Integer numero) {
		return Counter.esMultiploDe(numero, 2);
	}
	
	public static Boolean esMultiploDe(Integer numero, Integer cantidad) {
		return numero % cantidad == 0;
	}
	
}