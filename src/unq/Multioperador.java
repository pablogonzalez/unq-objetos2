package unq;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.RandomAccess;
import java.util.stream.Stream;

public class Multioperador{

	
	private ArrayList<Integer> numeros;
	
	public Multioperador() {
		this.numeros = new ArrayList<Integer>();
	}
	
	public void addNumero(Integer numero) {
		this.getNumeros().add(numero);
	}
	
	private ArrayList<Integer> getNumeros() {
		return this.numeros;
	}
	
	private Boolean hayNumeros() {
		return this.getNumeros().size() > 0;
	}
	
	public Integer sumar() {
		return this.hayNumeros() ? this.getNumeros().stream().reduce((a, b) -> a + b).get() : 0;
	}
	
	public Integer restar() {
		return this.hayNumeros() ? this.getNumeros().stream().reduce((a, b) -> a - b).get() : 0;	
	}
	
	public Integer multiplicar() {
		return this.hayNumeros() ? this.getNumeros().stream().reduce((a, b) -> a * b).get() : 0;	
	}
}
