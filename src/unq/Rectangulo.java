package unq;

public class Rectangulo{
	
	private Punto ubicacion;
	private Integer ancho;
	private Integer alto;
	
	/*protected Rectangulo() {
		this.setUbicacion(new Punto());
		this.setAncho(0);
		this.setAlto(0);
	}*/
	
	public Rectangulo(Punto ubicacion, Integer ancho, Integer alto) {
		this.setUbicacion(ubicacion);
		this.setAncho(ancho);
		this.setAlto(alto);
	}
	
	public Integer getAlto() {
		return alto;
	}

	protected void setAlto(Integer alto) {
		this.alto = alto;
	}

	public Integer getAncho() {
		return ancho;
	}

	protected void setAncho(Integer ancho) {
		this.ancho = ancho;
	}

	public Punto getUbicacion() {
		return ubicacion;
	}

	protected void setUbicacion(Punto ubicacion) {
		this.ubicacion = ubicacion;
	}
	
	public Integer getArea() {
		return this.getAlto() * this.getAncho();
	}
	
	public Integer getPerimetro() {
		return (this.getAlto() + this.getAncho()) * 2;
	}

	public Boolean esHorizontal() {
		return this.getAlto() < this.getAncho();
	}
	
	public Boolean esVertical() {
		return this.getAlto() > this.getAncho();
	}
}