package unq;

public class Punto {

	private Integer x;
	private Integer y;
	
	public Punto() {
		this(0, 0);
	}
	
	public Punto(Integer x, Integer y) {
		this.reubicar(x, y);
	}
	
	public void reubicar(Integer x, Integer y) {
		this.setX(x);
		this.setY(y);
	}
	
	public void setX(Integer x) {
		this.x = x;
	}
	
	public void setY(Integer y) {
		this.y = y;
	}
	
	public Punto sumar(Punto unPunto) {
		return new Punto(this.getX() + unPunto.getX(), this.getY() + unPunto.getY());
	}

	private Integer getY() {
		// TODO Auto-generated method stub
		return this.y;
	}

	private Integer getX() {
		// TODO Auto-generated method stub
		return this.x;
	}
	
}
