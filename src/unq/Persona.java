package unq;

import java.util.Date;

public class Persona {
	private String nombre;
	private Date fechaDeNacimiento;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Date getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}
	public void setFechaDeNacimiento(Date fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}
	
	public Integer getEdad() {
		return 30;
	}
	
	public Boolean menorQue(Persona unaPersona) {
		return this.getFechaDeNacimiento().before( unaPersona.getFechaDeNacimiento() );
	}
}
