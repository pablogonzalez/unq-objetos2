package composite.shapeshifter;

import java.util.List;

public interface IShapeShifter {
	public IShapeShifter compose(IShapeShifter shapeShifter);
	public Integer deepest();
	public IShapeShifter flat();
	public List<Integer> values();
}
