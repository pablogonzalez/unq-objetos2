package composite.shapeshifter;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ShapeShifter implements IShapeShifter {

	private List<IShapeShifter> shapeShifters;
	
	public ShapeShifter(List<IShapeShifter> shapeShifters) {
		setShapeShifters(shapeShifters);
	}

	@Override
	public IShapeShifter compose(IShapeShifter shapeShifter) {
		return ShapeFactory.newShape(this, shapeShifter);
	}

	@Override
	public Integer deepest() {
		Integer deep = 0;
		for (IShapeShifter shapeShifter:getShapeShifters()) {
			deep = shapeShifter.deepest() > deep ? shapeShifter.deepest() : deep;
		}	
		return deep + 1;		
	}

	@Override
	public IShapeShifter flat() {
		return ShapeFactory.newShape( values() );					
	};

	@Override
	public List<Integer> values() {
		ArrayList<Integer> lista = new ArrayList<Integer>();
		for (IShapeShifter shapeShifter:getShapeShifters()) {
			lista.addAll(shapeShifter.values());
		}	
		return lista;					
		
	};
	
	private List<IShapeShifter> getShapeShifters() {
		return shapeShifters;
	}

	private void setShapeShifters(List<IShapeShifter> shapeShifters) {
		this.shapeShifters = shapeShifters;
	}

	
}
