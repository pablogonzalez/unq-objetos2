package composite.shapeshifter;

import java.util.List;

public class Shape implements IShapeShifter{

	private int value;
	
	public Shape(Integer value) {
		setValue(value);
	}

	@Override
	public IShapeShifter compose(IShapeShifter shapeShifter) {
		return ShapeFactory.newShape(this, shapeShifter);
	}

	@Override
	public Integer deepest() {
		return 0;
	}

	@Override
	public IShapeShifter flat() {
		return this;
	}

	@Override
	public List<Integer> values() {
		return List.of(getValue());
	}

	private int getValue() {
		return value;
	}

	private void setValue(int value) {
		this.value = value;
	}

}
