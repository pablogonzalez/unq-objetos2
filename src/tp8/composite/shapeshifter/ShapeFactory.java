package composite.shapeshifter;

import java.util.List;
import java.util.stream.Collectors;

public class ShapeFactory {

	public static IShapeShifter  newShape(Integer value) {
    	return new Shape( value );
	}
	
	public static IShapeShifter  newShape(Integer... values) {
    	return new ShapeShifter( List.of( values ).stream().map((i) -> new Shape(i)).collect(Collectors.toList()) ) ;
	}
    
	public static IShapeShifter  newShape(List<Integer> values) {
    	return new ShapeShifter( values.stream().map((i) -> new Shape(i)).collect(Collectors.toList()) ) ;
	}
	
	public static IShapeShifter  newShape(IShapeShifter... shapeShifters) {
    	return new ShapeShifter( List.of(shapeShifters) ) ;
	}
	

	
}
