package encuentrosDeportivos;

public interface ISujetoSistema{

	public void agregar(IObservadorSistema observador);
	public void quitar(IObservadorSistema observador);
	public void notificar(EncuentroDeportivo encuentro);
	
	
}
