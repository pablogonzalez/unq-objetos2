package encuentrosDeportivos;

import java.util.List;

public class EncuentroDeportivo {
	private String deporte;
	private List<String> contrincantes;
	private String resultado;
	
	/**
	 * 
	 * @param resultado
	 * @param deporte
	 * @param contrincantes
	 */
	public EncuentroDeportivo(String resultado, String deporte, List<String> contrincantes) {
		setResultado(resultado);
		setDeporte(deporte);
		setContrincantes(contrincantes);
	}
	
	public void agregarContrincante(String equipo) {
		getContrincantes().add(equipo);
	}
	
	public Boolean tieneContrincante(String equipo) {
		return getContrincantes().contains(equipo);
	}
	
	private List<String> getContrincantes() {
		return contrincantes;
	}

	private void setContrincantes(List<String> contrincantes) {
		this.contrincantes = contrincantes;
	}

	public String getResultado() {
		return resultado;
	}

	public void setResultado(String resultado) {
		this.resultado = resultado;
	}

	public String getDeporte() {
		return deporte;
	}

	public void setDeporte(String deporte) {
		this.deporte = deporte;
	}
		
}
