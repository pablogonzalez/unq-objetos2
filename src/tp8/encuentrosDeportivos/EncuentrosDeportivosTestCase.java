package encuentrosDeportivos;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EncuentrosDeportivosTestCase {


	Servidor servidor1;
	Servidor servidor2;
	AplicacionMovil aplicacion1;
	AplicacionMovil aplicacion2;
	
	Sistema sistema;
	
	@BeforeEach
	void setUp() throws Exception {
		
		
		
		sistema = new Sistema();
		
		
		//servidor1 le interesan los encuentros de Tenis
		servidor1 = new Servidor();
		
		CriteriosDeInteres criterios1 = new CriteriosDeInteres();
		criterios1.agregarCriterio(new CriterioDeInteresDeporte("Tenis"));
		servidor1.agregarCriterioDeInteres(criterios1);
		
		
		//servidor2 le interesan los encuentros de Futbol y Basquet
		servidor2 =  new Servidor();
		servidor2.agregarCriterioDeInteres(new CriterioDeInteresDeporte("Futbol"));
		servidor2.agregarCriterioDeInteres(new CriterioDeInteresDeporte("Basquet"));
		
		
		//aplicacion1 le interesan los encuentros de Boca en Futbol
		aplicacion1 =  new AplicacionMovil();
		CriteriosDeInteres criterios3 = new CriteriosDeInteres();
		criterios3.agregarCriterio(new CriterioDeInteresEquipo("Boca"));
		criterios3.agregarCriterio(new CriterioDeInteresDeporte("Futbol"));
		aplicacion1.agregarCriterioDeInteres(criterios3);

		//aplicacion2 le interesa cualquier cosa
		aplicacion2 =  new AplicacionMovil(new CriteriosDeInteres());
		
		servidor1.subscribirseASistema(sistema);
		servidor2.subscribirseASistema(sistema);
		aplicacion1.subscribirseASistema(sistema);
		aplicacion2.subscribirseASistema(sistema);
		
		
		EncuentroDeportivo encuentro1 = mock(EncuentroDeportivo.class);
		when(encuentro1.tieneContrincante("Boca")).thenReturn(true);
		when(encuentro1.getDeporte()).thenReturn("Futbol");
			
		EncuentroDeportivo encuentro2 = mock(EncuentroDeportivo.class);
		when(encuentro2.tieneContrincante("Boca")).thenReturn(true);
		when(encuentro2.getDeporte()).thenReturn("Tenis");
		
		EncuentroDeportivo encuentro3 = mock(EncuentroDeportivo.class);
		when(encuentro3.tieneContrincante("Boca")).thenReturn(true);
		when(encuentro3.getDeporte()).thenReturn("Basquet");
		
		EncuentroDeportivo encuentro4 = mock(EncuentroDeportivo.class);
		when(encuentro4.tieneContrincante("Boca")).thenReturn(true);
		when(encuentro4.getDeporte()).thenReturn("Boxeo");

		EncuentroDeportivo encuentro5 = mock(EncuentroDeportivo.class);
		when(encuentro5.tieneContrincante("Boca")).thenReturn(false);
		when(encuentro5.getDeporte()).thenReturn("Futbol");
			
		EncuentroDeportivo encuentro6 = mock(EncuentroDeportivo.class);
		when(encuentro6.tieneContrincante("Boca")).thenReturn(false);
		when(encuentro6.getDeporte()).thenReturn("Tenis");
		
		EncuentroDeportivo encuentro7 = mock(EncuentroDeportivo.class);
		when(encuentro7.tieneContrincante("Boca")).thenReturn(false);
		when(encuentro7.getDeporte()).thenReturn("Basquet");
		
		EncuentroDeportivo encuentro8 = mock(EncuentroDeportivo.class);
		when(encuentro8.tieneContrincante("Boca")).thenReturn(false);
		when(encuentro8.getDeporte()).thenReturn("Boxeo");
		
		sistema.agregarEncuentro( encuentro1);
		sistema.agregarEncuentro( encuentro2);
		sistema.agregarEncuentro( encuentro3);
		sistema.agregarEncuentro( encuentro4);
		sistema.agregarEncuentro( encuentro5);
		sistema.agregarEncuentro( encuentro6);
		sistema.agregarEncuentro( encuentro7);
		sistema.agregarEncuentro( encuentro8);
	
	}

	/**
	 *servidor1 le interesan los encuentros de Tenis
	 */
	@Test
	void testInvestigador1() {
		assertEquals(2, servidor1.getVecesQueRecibioNotificacion());
	}

	/**
	 *servidor2 le interesan los encuentros de Futbol y Basquet
	 */
	@Test
	void testInvestigador2() {
		assertEquals(4, servidor2.getVecesQueRecibioNotificacion());
	}

	/**
	 *aplicacion1 le interesan los encuentros de Boca en Futbol
	 */
	@Test
	void testInvestigador3() {
		assertEquals(1, aplicacion1.getVecesQueRecibioNotificacion());
	}

	/**
	 *aplicacion2 le interesa cualquier cosa
	 */
	@Test
	void testInvestigador4() {
		assertEquals(8, aplicacion2.getVecesQueRecibioNotificacion());
	}
}
