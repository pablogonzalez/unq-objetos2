package encuentrosDeportivos;

public class CriterioDeInteresEquipo extends CriteriosDeInteres {
	public String equipo;
	
	public CriterioDeInteresEquipo(String equipo) {
		this.equipo = equipo;
	}
	@Override
	public Boolean validarCriterio(EncuentroDeportivo encuentro) {
		return encuentro.tieneContrincante(equipo);
	}

}