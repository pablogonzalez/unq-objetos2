package encuentrosDeportivos;

import java.util.ArrayList;
import java.util.List;

public class CriteriosDeInteres implements ICriterioDeInteres {
	
	private List<ICriterioDeInteres> criterios = null;
	
	public CriteriosDeInteres() {
		
	}
	
	public CriteriosDeInteres(List<ICriterioDeInteres> criterios) {
		setCriterios(criterios);
	}
	

	public final Boolean esEncuentroDeInteres(EncuentroDeportivo encuentro) {
		return validarCriterio(encuentro) && getCriterios().stream().allMatch(criterio -> criterio.esEncuentroDeInteres(encuentro));
	}
	
	public Boolean validarCriterio(EncuentroDeportivo encuentro) {
		return true;
	}

	public void agregarCriterio(ICriterioDeInteres criterio) {
		getCriterios().add(criterio);
	}
	
	private List<ICriterioDeInteres> getCriterios() {
		if(criterios == null) {
			setCriterios( new ArrayList<ICriterioDeInteres>() );
		}
		return criterios;
	}

	private void setCriterios(List<ICriterioDeInteres> criterios) {
		this.criterios = criterios;
	}
}
