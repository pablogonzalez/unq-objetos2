package encuentrosDeportivos;

import java.util.ArrayList;
import java.util.List;

public class AplicacionMovil  implements IObservadorSistema{

	private List<ICriterioDeInteres> criteriosDeInteres;
	private int vecesQueRecibioNotificacion;
	
	/**
	 * Instancia una AplicacionMovil
	 */
	public AplicacionMovil() {
		this.setCriteriosDeInteres(new ArrayList<ICriterioDeInteres>());
	}
	
	/**
	 * Instancia una AplicacionMovil con un unico criterio de interes
	 */
	public AplicacionMovil(ICriterioDeInteres criterios) {
		this.setCriteriosDeInteres(new ArrayList<ICriterioDeInteres>());
		this.agregarCriterioDeInteres(criterios);
	}

	/**
	 * Instancia una AplicacionMovil con criterios de interes
	 */
	public AplicacionMovil(List<ICriterioDeInteres> criterios) {
		this.setCriteriosDeInteres(criterios);
	}

	public void subscribirseASistema(Sistema sistema) {
		sistema.agregar(this);
	}
	
	/**
	 * Agrega un criterio de interes a la lista
	 * @param criterio
	 */
	public void agregarCriterioDeInteres(ICriterioDeInteres criterio) {
		criteriosDeInteres.add(criterio);
	}
	
	/**
	 * retorna una copia de la lista de criterios de interes
	 */
	@Override
	public List<ICriterioDeInteres> getCriteriosDeInteres() {
		return List.copyOf( criteriosDeInteres ) ;
	}

	/**
	 * Setea la lista de criterios de interes de manera local
	 * @param criteriosDeInteres
	 */
	private void setCriteriosDeInteres(List<ICriterioDeInteres> criteriosDeInteres) {
		this.criteriosDeInteres = criteriosDeInteres;
	}

	@Override
	public void actualizar(EncuentroDeportivo encuentro) {
		vecesQueRecibioNotificacion++;
	}

	public int getVecesQueRecibioNotificacion() {
		return vecesQueRecibioNotificacion;
	}



}
