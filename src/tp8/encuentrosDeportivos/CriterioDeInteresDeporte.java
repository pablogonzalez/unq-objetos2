package encuentrosDeportivos;

public class CriterioDeInteresDeporte extends CriteriosDeInteres {
	public String deporte;
	
	public CriterioDeInteresDeporte(String deporte) {
		this.deporte = deporte;
	}
	@Override
	public Boolean validarCriterio(EncuentroDeportivo encuentro) {
		return encuentro.getDeporte() == deporte;
	}

}
