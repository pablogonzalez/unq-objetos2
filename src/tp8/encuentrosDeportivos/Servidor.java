package encuentrosDeportivos;

import java.util.ArrayList;
import java.util.List;

public class Servidor implements IObservadorSistema{
	private List<ICriterioDeInteres> criteriosDeInteres;
	private int vecesQueRecibioNotificacion;
	
	/**
	 * Instancia un Servidor
	 */
	public Servidor() {
		this.setCriteriosDeInteres(new ArrayList<ICriterioDeInteres>());
	}
	
	/**
	 * Instancia un Servidor con un unico criterio de interes
	 */
	public Servidor(ICriterioDeInteres criterios) {
		this.setCriteriosDeInteres(new ArrayList<ICriterioDeInteres>());
		this.agregarCriterioDeInteres(criterios);
	}

	/**
	 * Instancia un Servidor con criterios de interes
	 */
	public Servidor(List<ICriterioDeInteres> criterios) {
		this.setCriteriosDeInteres(criterios);
	}

	public void subscribirseASistema(Sistema sistema) {
		sistema.agregar(this);
	}
	
	/**
	 * Agrega un criterio de interes a la lista
	 * @param criterio
	 */
	public void agregarCriterioDeInteres(ICriterioDeInteres criterio) {
		criteriosDeInteres.add(criterio);
	}
	
	/**
	 * retorna una copia de la lista de criterios de interes
	 */
	@Override
	public List<ICriterioDeInteres> getCriteriosDeInteres() {
		return List.copyOf( criteriosDeInteres ) ;
	}

	/**
	 * Setea la lista de criterios de interes de manera local
	 * @param criteriosDeInteres
	 */
	private void setCriteriosDeInteres(List<ICriterioDeInteres> criteriosDeInteres) {
		this.criteriosDeInteres = criteriosDeInteres;
	}

	@Override
	public void actualizar(EncuentroDeportivo encuentro) {
		vecesQueRecibioNotificacion++;
	}

	public int getVecesQueRecibioNotificacion() {
		return vecesQueRecibioNotificacion;
	}



	
}
