package encuentrosDeportivos;

public interface ICriterioDeInteres {
	/**
	 * Valida el encuentro para el Criterio local
	 * @param encuentro
	 * @return
	 */
	public Boolean validarCriterio(EncuentroDeportivo encuentro);
	/**
	 * Recursivamente valida si el encuentro es de interes segun la lista de criterios agregados
	 * @param encuentro
	 * @return
	 */
	public Boolean esEncuentroDeInteres(EncuentroDeportivo encuentro);
}
