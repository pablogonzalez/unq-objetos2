package encuentrosDeportivos;

import java.util.List;

public interface IObservadorSistema  {
	public List<ICriterioDeInteres> getCriteriosDeInteres() ;
	public void actualizar(EncuentroDeportivo encuentro);
}
