package encuentrosDeportivos;

import java.util.ArrayList;
import java.util.List;

/**
 * Tiene la responsabilidad de contener todas las publicaciones y 
 * notificar a los observadores cuando se crea una nueva publicacion que les interesa
 * @author PabloGabriel
 *
 */
public class Sistema implements ISujetoSistema {

	private List<IObservadorSistema> observadores;
	private List<EncuentroDeportivo> encuentros;
	
	/**
	 * Crea una instancia de la clase
	 */
	public Sistema() {
		this.setObservadores(new ArrayList<IObservadorSistema>());
		this.setEncuentros(new ArrayList<EncuentroDeportivo>());
	}
	
	/**
	 * Agrega un observador a la lista
	 */
	@Override
	public void agregar(IObservadorSistema observador) {
		this.getObservadores().add(observador);
	}

	/**
	 * Quita un observador de la lista
	 */
	@Override
	public void quitar(IObservadorSistema observador) {
		this.getObservadores().remove(observador);
	}

	/**
	 * Filtra los observadores que le interesa la publicacion y los notifica
	 */
	@Override
	public void notificar(EncuentroDeportivo publicacion) {
		this.getObservadores().stream()
		.filter(
			observador -> this.esPublicacionDeInteres(observador, publicacion)
		)
		.forEach(
			observador -> observador.actualizar(publicacion)
		);
	}
	
	/**
	 * Valida si el encuentro cumple con alguno de los criterios de interes dados por el observador
	 * @param observador
	 * @param encuentro
	 * @return
	 */
	private Boolean esPublicacionDeInteres(IObservadorSistema observador, EncuentroDeportivo encuentro) {
		return observador.getCriteriosDeInteres().stream().anyMatch( criterio -> criterio.esEncuentroDeInteres(encuentro) );
	}
	
	/**
	 * Agrega un encuentro a la lista
	 * @param encuentro
	 */
	public void agregarEncuentro(EncuentroDeportivo encuentro) {
		this.getEncuentros().add(encuentro);
		this.notificar(encuentro);
	}

	/**
	 * retorna la lista de observadores de manera local
	 * @return
	 */
	private List<IObservadorSistema> getObservadores() {
		return observadores;
	}

	/**
	 * setea la lista de observadores de manera local
	 * @param investigadoresObservers
	 */
	private void setObservadores(List<IObservadorSistema> investigadoresObservers) {
		this.observadores = investigadoresObservers;
	}

	/**
	 * retorna la lista de encuentros de manera local
	 * @return
	 */
	private List<EncuentroDeportivo> getEncuentros() {
		return encuentros;
	}

	/**
	 * Setea la lista de encuentros de manera local
	 * @param publicaciones
	 */
	private void setEncuentros(List<EncuentroDeportivo> encuentros) {
		this.encuentros = encuentros;
	}


}
