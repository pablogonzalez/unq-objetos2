package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import composite.shapeshifter.IShapeShifter;
import composite.shapeshifter.ShapeFactory;


class ShapeShifterTestCase {

	IShapeShifter a;
	IShapeShifter b;
	IShapeShifter c;
	IShapeShifter d;
	IShapeShifter e;
	IShapeShifter f;
	IShapeShifter g;
	
	@BeforeEach
	void setUp() throws Exception {
		a = ShapeFactory.newShape(1);
		b = ShapeFactory.newShape(2);
		c = a.compose(b);
		d = ShapeFactory.newShape(3);
		d = d.compose(c);
		e = ShapeFactory.newShape( 5, 6 );
		f = d.compose(e);
		g = f.flat();
	}

	@Test
	void testValues() {
		assertEquals(List.of(1), a.values());
		assertEquals(List.of(2), b.values());
		assertEquals(List.of(1, 2), c.values());
		assertEquals(List.of(3, 1, 2), d.values());
		assertEquals(List.of(5, 6), e.values());
		assertEquals(List.of(3, 1, 2, 5, 6), f.values());
		assertEquals(List.of(3, 1, 2, 5, 6), g.values());
	}
	
	@Test
	void testDeepest() {
		assertEquals(0, a.deepest());
		assertEquals(0, b.deepest());
		assertEquals(1, c.deepest());
		assertEquals(2, d.deepest());
		assertEquals(1, e.deepest());
		assertEquals(3, f.deepest());
		assertEquals(1, g.deepest());
	}

}
