package observer.publicaciones;

import java.util.ArrayList;
import java.util.List;

public class CriteriosDeInteres implements ICriterioDeInteres {
	
	private List<ICriterioDeInteres> criterios = null;
	
	public CriteriosDeInteres() {
		
	}
	
	public CriteriosDeInteres(List<ICriterioDeInteres> criterios) {
		setCriterios(criterios);
	}
	

	public final Boolean esPublicacionDeInteres(Publicacion publicacion) {
		return validarCriterio(publicacion) && getCriterios().stream().allMatch(criterio -> criterio.esPublicacionDeInteres(publicacion));
	}
	
	public Boolean validarCriterio(Publicacion publicacion) {
		return true;
	}

	public void agregarCriterio(ICriterioDeInteres criterio) {
		getCriterios().add(criterio);
	}
	
	private List<ICriterioDeInteres> getCriterios() {
		if(criterios == null) {
			setCriterios( new ArrayList<ICriterioDeInteres>() );
		}
		return criterios;
	}

	private void setCriterios(List<ICriterioDeInteres> criterios) {
		this.criterios = criterios;
	}
}
