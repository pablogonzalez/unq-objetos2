package observer.publicaciones;

import java.util.List;

public class Publicacion {
	private String titulo;
	private List<String> autores;
	private String filiacion;
	private String tipoDeArticulo;
	private String lugarDePublicacion;
	private List<String> palabrasClaves;
	
	/**
	 * 
	 * @param titulo
	 * @param autores
	 * @param filiacion
	 * @param tipoDeArticulo
	 * @param lugarDePublicacion
	 * @param palabrasClaves
	 */
	public Publicacion(String titulo, List<String> autores, String filiacion, String tipoDeArticulo, String lugarDePublicacion, List<String> palabrasClaves) {
		setTitulo(titulo);
		setAutores(autores);
		setFiliacion(filiacion);
		setTipoDeArticulo(tipoDeArticulo);
		setLugarDePublicacion(lugarDePublicacion);
		setPalabrasClaves(palabrasClaves);
	}
	
	public void agregarPalabraClave(String palabraClave) {
		getPalabrasClaves().add(palabraClave);
	}
	
	public Boolean tienePalabraClave(String palabraClave) {
		return getPalabrasClaves().contains(palabraClave);
	}
	
	public Boolean tieneAutor(String autor) {
		return getAutores().contains(autor);
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	private List<String> getAutores() {
		return autores;
	}
	private void setAutores(List<String> autores) {
		this.autores = autores;
	}
	public String getFiliacion() {
		return filiacion;
	}
	public void setFiliacion(String filiacion) {
		this.filiacion = filiacion;
	}
	public String getTipoDeArticulo() {
		return tipoDeArticulo;
	}
	public void setTipoDeArticulo(String tipoDeArticulo) {
		this.tipoDeArticulo = tipoDeArticulo;
	}
	public String getLugarDePublicacion() {
		return lugarDePublicacion;
	}
	public void setLugarDePublicacion(String lugarDePublicacion) {
		this.lugarDePublicacion = lugarDePublicacion;
	}
	private List<String> getPalabrasClaves() {
		return palabrasClaves;
	}
	private void setPalabrasClaves(List<String> palabrasClaves) {
		this.palabrasClaves = palabrasClaves;
	}
	
}
