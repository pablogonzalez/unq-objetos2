package observer.publicaciones;

public interface ISujetoPublicaciones{

	public void agregar(IObservadorPublicaciones observador);
	public void quitar(IObservadorPublicaciones observador);
	public void notificar(Publicacion publicacion);
	
	
}
