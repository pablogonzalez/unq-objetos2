package observer.publicaciones;

public interface ICriterioDeInteres {
	/**
	 * Valida la publicacion para el Criterio local
	 * @param publicacion
	 * @return
	 */
	public Boolean validarCriterio(Publicacion publicacion);
	/**
	 * Recursivamente valida si la publicacion es de interes segun la lista de criterios agregados
	 * @param publicacion
	 * @return
	 */
	public Boolean esPublicacionDeInteres(Publicacion publicacion);
}
