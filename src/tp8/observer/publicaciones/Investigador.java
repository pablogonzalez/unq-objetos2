package observer.publicaciones;

import java.util.ArrayList;
import java.util.List;

public class Investigador implements IObservadorPublicaciones{
	private List<ICriterioDeInteres> criteriosDeInteres;
	private int vecesQueRecibioNotificacion;
	
	/**
	 * Instancia un Investigador
	 */
	public Investigador() {
		this.setCriteriosDeInteres(new ArrayList<ICriterioDeInteres>());
	}
	
	/**
	 * Instancia un Investigador con un unico criterio de interes
	 */
	public Investigador(ICriterioDeInteres criterios) {
		this.setCriteriosDeInteres(new ArrayList<ICriterioDeInteres>());
		this.agregarCriterioDeInteres(criterios);
	}

	/**
	 * Instancia un Investigador con criterios de interes
	 */
	public Investigador(List<ICriterioDeInteres> criterios) {
		this.setCriteriosDeInteres(criterios);
	}

	public void subscribirseAPublicaciones(PublicacionesBibliograficas publicaciones) {
		publicaciones.agregar(this);
	}
	
	/**
	 * Agrega un criterio de interes a la lista
	 * @param criterio
	 */
	public void agregarCriterioDeInteres(ICriterioDeInteres criterio) {
		criteriosDeInteres.add(criterio);
	}
	
	/**
	 * retorna una copia de la lista de criterios de interes
	 */
	@Override
	public List<ICriterioDeInteres> getCriteriosDeInteres() {
		return List.copyOf( criteriosDeInteres ) ;
	}

	/**
	 * Setea la lista de criterios de interes de manera local
	 * @param criteriosDeInteres
	 */
	private void setCriteriosDeInteres(List<ICriterioDeInteres> criteriosDeInteres) {
		this.criteriosDeInteres = criteriosDeInteres;
	}

	/**
	 * publica un articulo en una instancia de PublicacionesBibliograficas
	 * @param publicaciones
	 * @param publicacion
	 */
	public void publicarArticulo(PublicacionesBibliograficas publicaciones, Publicacion publicacion) {
		publicaciones.agregarPublicacion(publicacion);
	}
	

	@Override
	public void actualizar(Publicacion publicacion) {
		vecesQueRecibioNotificacion++;
	}

	public int getVecesQueRecibioNotificacion() {
		return vecesQueRecibioNotificacion;
	}



	
}
