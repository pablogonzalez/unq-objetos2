package observer.publicaciones;

public class CriterioDeInteresLugarDePublicacion extends CriteriosDeInteres {
	private String lugarDePublicacion;
	public CriterioDeInteresLugarDePublicacion(String lugarDePublicacion) {
		this.lugarDePublicacion = lugarDePublicacion;
	}
	@Override
	public Boolean validarCriterio(Publicacion publicacion) {
		return publicacion.getLugarDePublicacion() == lugarDePublicacion;
	}

}
