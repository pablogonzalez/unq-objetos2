package observer.publicaciones;

public class CriterioDeInteresPalabraClave extends CriteriosDeInteres {
	private String palabraClave;
	
	public CriterioDeInteresPalabraClave(String palabraClave) {
		this.palabraClave = palabraClave;
	}
	@Override
	public Boolean validarCriterio(Publicacion publicacion) {
		return publicacion.tienePalabraClave(palabraClave);
	}

}