package observer.publicaciones;

import java.util.ArrayList;
import java.util.List;

/**
 * Tiene la responsabilidad de contener todas las publicaciones y 
 * notificar a los observadores cuando se crea una nueva publicacion que les interesa
 * @author PabloGabriel
 *
 */
public class PublicacionesBibliograficas implements ISujetoPublicaciones {

	private List<IObservadorPublicaciones> observadores;
	private List<Publicacion> publicaciones;
	
	/**
	 * Crea una instancia de la clase
	 */
	public PublicacionesBibliograficas() {
		this.setObservadores(new ArrayList<IObservadorPublicaciones>());
		this.setPublicaciones(new ArrayList<Publicacion>());
	}
	
	/**
	 * Agrega un observador a la lista
	 */
	@Override
	public void agregar(IObservadorPublicaciones observador) {
		this.getObservadores().add(observador);
	}

	/**
	 * Quita un observador de la lista
	 */
	@Override
	public void quitar(IObservadorPublicaciones observador) {
		this.getObservadores().remove(observador);
	}

	/**
	 * Filtra los observadores que le interesa la publicacion y los notifica
	 */
	@Override
	public void notificar(Publicacion publicacion) {
		this.getObservadores().stream()
		.filter(
			observador -> this.esPublicacionDeInteres(observador, publicacion)
		)
		.forEach(
			observador -> observador.actualizar(publicacion)
		);
	}
	
	/**
	 * Valida si la publicacion cumple con alguno de los criterios de interes dados por el observador
	 * @param observador
	 * @param publicacion
	 * @return
	 */
	private Boolean esPublicacionDeInteres(IObservadorPublicaciones observador, Publicacion publicacion) {
		return observador.getCriteriosDeInteres().stream().anyMatch( criterio -> criterio.esPublicacionDeInteres(publicacion) );
	}
	
	/**
	 * Agrega una publicacion a la lista
	 * @param publicacion
	 */
	public void agregarPublicacion(Publicacion publicacion) {
		this.getPublicaciones().add(publicacion);
		this.notificar(publicacion);
	}

	/**
	 * retorna la lista de observadores de manera local
	 * @return
	 */
	private List<IObservadorPublicaciones> getObservadores() {
		return observadores;
	}

	/**
	 * setea la lista de observadores de manera local
	 * @param investigadoresObservers
	 */
	private void setObservadores(List<IObservadorPublicaciones> investigadoresObservers) {
		this.observadores = investigadoresObservers;
	}

	/**
	 * retorna la lista de publicaciones de manera local
	 * @return
	 */
	private List<Publicacion> getPublicaciones() {
		return publicaciones;
	}

	/**
	 * Setea la lista de publicaciones de manera local
	 * @param publicaciones
	 */
	private void setPublicaciones(List<Publicacion> publicaciones) {
		this.publicaciones = publicaciones;
	}


}
