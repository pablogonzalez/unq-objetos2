package observer.publicaciones;

public class CriterioDeInteresTipoDeArticulo extends CriteriosDeInteres {

	public String tipoDeArticulo;
	public CriterioDeInteresTipoDeArticulo(String tipoDeArticulo) {
		this.tipoDeArticulo = tipoDeArticulo;
	}
	@Override
	public Boolean validarCriterio(Publicacion publicacion) {
		return publicacion.getTipoDeArticulo() == tipoDeArticulo;
	}
	
	

}
