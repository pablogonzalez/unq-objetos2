package observer.publicaciones;

public class CriterioDeInteresAutor extends CriteriosDeInteres {
	public String autor;
	
	public CriterioDeInteresAutor(String autor) {
		super();
		this.autor = autor;
	}
	@Override
	public Boolean validarCriterio(Publicacion publicacion) {
		return publicacion.tieneAutor(autor);
	}

}
