package observer.publicaciones;

public class CriterioDeInteresFiliacion extends CriteriosDeInteres {
	private String filiacion;
	public CriterioDeInteresFiliacion(String filiacion) {
		this.filiacion = filiacion;
	}
	@Override
	public Boolean validarCriterio(Publicacion publicacion) {
		return publicacion.getFiliacion() == filiacion;
	}

}
