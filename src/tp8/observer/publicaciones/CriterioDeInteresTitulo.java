package observer.publicaciones;

public class CriterioDeInteresTitulo extends CriteriosDeInteres {
	public String titulo;
	
	public CriterioDeInteresTitulo(String titulo) {
		this.titulo = titulo;
	}
	@Override
	public Boolean validarCriterio(Publicacion publicacion) {
		return publicacion.getTitulo() == titulo;
	}

}