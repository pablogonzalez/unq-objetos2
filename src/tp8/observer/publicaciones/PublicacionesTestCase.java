package observer.publicaciones;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PublicacionesTestCase {

	Investigador investigadorQuePublica;
	Investigador investigador1;
	Investigador investigador2;
	Investigador investigador3;
	Investigador investigador4;
	
	PublicacionesBibliograficas publicaciones;
	
	@BeforeEach
	void setUp() throws Exception {
		
		
		
		publicaciones = new PublicacionesBibliograficas();
		
		
		investigadorQuePublica = new Investigador();
		
		//investigador1 le interesan las publicaciones de Smalltalks que tengan como autor a Martin (por lo menos)
		investigador1 = new Investigador();
		
		CriteriosDeInteres criterios1 = new CriteriosDeInteres();
		criterios1.agregarCriterio(new CriterioDeInteresPalabraClave("Smalltalks"));
		criterios1.agregarCriterio(new CriterioDeInteresAutor("Martin"));
		investigador1.agregarCriterioDeInteres(criterios1);
		
		
		//investigador2 le interesan las publicaciones de Smalltalks que tengan como autor a Martin o a Diego (por lo menos)
		investigador2 =  new Investigador();
		CriteriosDeInteres criterios2 = new CriteriosDeInteres();
		criterios2.agregarCriterio(new CriterioDeInteresPalabraClave("Smalltalks"));
		criterios2.agregarCriterio(new CriterioDeInteresAutor("Diego"));
		investigador2.agregarCriterioDeInteres(criterios1);
		investigador2.agregarCriterioDeInteres(criterios2);
		
		
		//investigador3 le interesan las publicaciones  de Smalltalks que tengan como autores a Martin y a Diego (por lo menos)
		investigador3 =  new Investigador();
		CriteriosDeInteres criterios3 = new CriteriosDeInteres();
		criterios3.agregarCriterio(new CriterioDeInteresPalabraClave("Smalltalks"));
		criterios3.agregarCriterio(new CriterioDeInteresAutor("Diego"));
		criterios3.agregarCriterio(new CriterioDeInteresAutor("Martin"));
		investigador3.agregarCriterioDeInteres(criterios3);

		//investigador4 le interesa cualquier cosa
		investigador4 =  new Investigador(new CriteriosDeInteres());
		
		investigador1.subscribirseAPublicaciones(publicaciones);
		investigador2.subscribirseAPublicaciones(publicaciones);
		investigador3.subscribirseAPublicaciones(publicaciones);
		investigador4.subscribirseAPublicaciones(publicaciones);
		
		
		Publicacion publicacion1 = mock(Publicacion.class);
		when(publicacion1.tienePalabraClave("Smalltalks")).thenReturn(false);
		when(publicacion1.tieneAutor("Martin")).thenReturn(true);
		when(publicacion1.tieneAutor("Diego")).thenReturn(false);
			
		Publicacion publicacion2 = mock(Publicacion.class);
		when(publicacion2.tienePalabraClave("Smalltalks")).thenReturn(false);
		when(publicacion2.tieneAutor("Martin")).thenReturn(false);
		when(publicacion2.tieneAutor("Diego")).thenReturn(true);
		
		Publicacion publicacion3 = mock(Publicacion.class);
		when(publicacion3.tienePalabraClave("Smalltalks")).thenReturn(true);
		when(publicacion3.tieneAutor("Martin")).thenReturn(true);
		when(publicacion3.tieneAutor("Diego")).thenReturn(false);
		
		Publicacion publicacion4 = mock(Publicacion.class);
		when(publicacion4.tienePalabraClave("Smalltalks")).thenReturn(true);
		when(publicacion4.tieneAutor("Martin")).thenReturn(false);
		when(publicacion4.tieneAutor("Diego")).thenReturn(true);

		Publicacion publicacion5 = mock(Publicacion.class);
		when(publicacion5.tienePalabraClave("Smalltalks")).thenReturn(false);
		when(publicacion5.tieneAutor("Martin")).thenReturn(true);
		when(publicacion5.tieneAutor("Diego")).thenReturn(true);

		Publicacion publicacion6 = mock(Publicacion.class);
		when(publicacion6.tienePalabraClave("Smalltalks")).thenReturn(true);
		when(publicacion6.tieneAutor("Martin")).thenReturn(true);
		when(publicacion6.tieneAutor("Diego")).thenReturn(true);
		
		Publicacion publicacion7 = mock(Publicacion.class);
		when(publicacion7.tienePalabraClave("Smalltalks")).thenReturn(false);
		when(publicacion7.tieneAutor("Martin")).thenReturn(false);
		when(publicacion7.tieneAutor("Diego")).thenReturn(false);

		Publicacion publicacion8 = mock(Publicacion.class);
		when(publicacion8.tienePalabraClave("Smalltalks")).thenReturn(true);
		when(publicacion8.tieneAutor("Martin")).thenReturn(false);
		when(publicacion8.tieneAutor("Diego")).thenReturn(false);

		
		investigadorQuePublica.publicarArticulo(publicaciones, publicacion1);
		investigadorQuePublica.publicarArticulo(publicaciones, publicacion2);
		investigadorQuePublica.publicarArticulo(publicaciones, publicacion3);
		investigadorQuePublica.publicarArticulo(publicaciones, publicacion4);
		investigadorQuePublica.publicarArticulo(publicaciones, publicacion5);
		investigadorQuePublica.publicarArticulo(publicaciones, publicacion6);
		investigadorQuePublica.publicarArticulo(publicaciones, publicacion7);
		investigadorQuePublica.publicarArticulo(publicaciones, publicacion8);
	
	}

	/**
	 *investigador1 le interesan las publicaciones de Smalltalks que tengan como autor a Martin (por lo menos)
	 */
	@Test
	void testInvestigador1() {
		assertEquals(2, investigador1.getVecesQueRecibioNotificacion());
	}

	/**
	 *investigador2 le interesan las publicaciones de Smalltalks que tengan como autor a Martin o a Diego (por lo menos)
	 */
	@Test
	void testInvestigador2() {
		assertEquals(3, investigador2.getVecesQueRecibioNotificacion());
	}

	/**
	 *investigador3 le interesan las publicaciones  de Smalltalks que tengan como autores a Martin y a Diego (por lo menos)
	 */
	@Test
	void testInvestigador3() {
		assertEquals(1, investigador3.getVecesQueRecibioNotificacion());
	}

	/**
	 *investigador4 le interesa cualquier cosa
	 */
	@Test
	void testInvestigador4() {
		assertEquals(8, investigador4.getVecesQueRecibioNotificacion());
	}
}
