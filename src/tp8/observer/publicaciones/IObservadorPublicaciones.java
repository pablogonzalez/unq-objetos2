package observer.publicaciones;

import java.util.List;

public interface IObservadorPublicaciones  {
	//public List<String> getTemasDeInteres();
	public List<ICriterioDeInteres> getCriteriosDeInteres() ;
	public void actualizar(Publicacion publicacion);
}
