package elementosSimilares;


public class PropiedadEnComun extends Filtro{

	@Override
	public Boolean esSimilar(WikipediaPage page1, WikipediaPage page2) {
		return  page1.getInfobox().keySet().stream().anyMatch(info -> page2.getInfobox().keySet().contains(info));
	}


}
