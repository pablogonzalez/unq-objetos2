package elementosSimilares;

public class LinkEnComun extends Filtro{

	@Override
	public Boolean esSimilar(WikipediaPage page1, WikipediaPage page2) {
		return page1.getLinks().stream().anyMatch(link -> page2.getLinks().contains(link));
	}



}
