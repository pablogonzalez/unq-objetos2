package elementosSimilares;

public class MismaLetraInicial extends Filtro{

	@Override
	public Boolean esSimilar(WikipediaPage page1, WikipediaPage page2) {
		return page1.getTitle().startsWith(page2.getTitle().substring(0, 1));
	}
	

}
