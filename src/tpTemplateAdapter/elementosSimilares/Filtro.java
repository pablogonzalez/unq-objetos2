package elementosSimilares;

import java.util.List;
import java.util.stream.Collectors;

public abstract class Filtro {
	
	public List<WikipediaPage> getSimilarPages(WikipediaPage page, List<WikipediaPage> wikipedia){
		return wikipedia.stream().filter(p -> this.esSimilar(p, page)).collect(Collectors.toList());
	}
	
	public abstract Boolean esSimilar ( WikipediaPage page1, WikipediaPage page2 );
	
}
