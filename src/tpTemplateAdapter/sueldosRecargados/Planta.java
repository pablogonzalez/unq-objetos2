package sueldosRecargados;

/**
 * Por	�ltimo	a	los	empleados	de	Planta	se	les	paga	un	sueldo	b�sico	de	$	3000	y	un	
 * plus	por	cada	hijo	que	posea	de	$	150	cada	hijo
 * @author PabloGabriel
 *
 */
public class Planta extends Empleado{

	private Float sueldoBasico;
	private Float valorHijo;
	private Integer cantidadHijos;
	
	public Planta(Integer cantidadHijos) {
		setSueldoBasico(3000F);
		setValorHijo(150F);
		setCantidadHijos(cantidadHijos);
	}
	
	@Override
	protected Float importeAsignacionesFamiliares() {
		return getValorHijo() * getCantidadHijos();
	}

	@Override
	protected Float importeHorasTrabajadas() {
		return 0F;
	}

	@Override
	protected Float importeSueldoBasico() {
		return getSueldoBasico();
	}

	public Float getSueldoBasico() {
		return sueldoBasico;
	}

	public void setSueldoBasico(Float sueldoBasico) {
		this.sueldoBasico = sueldoBasico;
	}

	public Float getValorHijo() {
		return valorHijo;
	}

	public void setValorHijo(Float valorHijo) {
		this.valorHijo = valorHijo;
	}

	public Integer getCantidadHijos() {
		return cantidadHijos;
	}

	public void setCantidadHijos(Integer cantidadHijos) {
		this.cantidadHijos = cantidadHijos;
	}


}
