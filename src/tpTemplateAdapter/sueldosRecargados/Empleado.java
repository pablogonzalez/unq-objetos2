package sueldosRecargados;

public abstract class Empleado {

	public final Float sueldo() {
		return sueldoBruto() - importeDescuentos() ;
	};
	
	public final Float sueldoBruto() {
		return importeSueldoBasico() + importeHorasTrabajadas() + importeAsignacionesFamiliares();
	}
	
	protected final Float importeDescuentos() {
		return sueldoBruto() * 0.13F;
	};

	protected abstract Float importeAsignacionesFamiliares();

	protected abstract Float importeHorasTrabajadas();

	protected abstract Float importeSueldoBasico();
	
}

