package sueldosRecargados;
/**
 * El	sueldo	de	un	empleado	Temporario	est� determinado	por	el	pago	
 * de	$5	por	hora	que	trabaj�	m�s	el	sueldo	b�sico	que	es	de	$1000,	adem�s	se	le	paga	$100	
 * si	posee	hijos	y/o	est�	casado
 * @author PabloGabriel
 *
 */
public class Temporario extends Empleado{

	private Float valorHora;
	private Float valorAsignacionFamiliar;
	private Float sueldoBasico;
	private Integer cantidadHijos;
	private Boolean estaCasado;
	private Integer horasTrabajadas;
	
	public Temporario(Integer cantidadHijos, Integer horasTrabajadas) {
		setValorHora(5F);
		setValorAsignacionFamiliar(100F);
		setSueldoBasico(1000F);
		setCantidadHijos(cantidadHijos);
		setHorasTrabajadas(horasTrabajadas);
	}

	@Override
	protected Float importeHorasTrabajadas() {
		return getHorasTrabajadas() * getValorHora();
	}

	@Override
	protected Float importeSueldoBasico() {
		return getSueldoBasico();
	}

	@Override
	protected Float importeAsignacionesFamiliares() {
		return getEstaCasado() || getTieneHijos() ? valorAsignacionFamiliar : 0 ;
	}
	
	private Boolean getTieneHijos() {
		return getCantidadHijos() > 0;
	}
	
	public Float getValorHora() {
		return valorHora;
	}

	public void setValorHora(Float valorHora) {
		this.valorHora = valorHora;
	}

	public Float getSueldoBasico() {
		return sueldoBasico;
	}

	public void setSueldoBasico(Float sueldoBasico) {
		this.sueldoBasico = sueldoBasico;
	}

	public Integer getCantidadHijos() {
		return cantidadHijos;
	}

	public void setCantidadHijos(Integer cantidadHijos) {
		this.cantidadHijos = cantidadHijos;
	}

	public Boolean getEstaCasado() {
		return estaCasado;
	}

	public void setEstaCasado(Boolean estaCasado) {
		this.estaCasado = estaCasado;
	}

	public Integer getHorasTrabajadas() {
		return horasTrabajadas;
	}

	public void setHorasTrabajadas(Integer horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}

	public Float getValorAsignacionFamiliar() {
		return valorAsignacionFamiliar;
	}

	public void setValorAsignacionFamiliar(Float valorAsignacionFamiliar) {
		this.valorAsignacionFamiliar = valorAsignacionFamiliar;
	}




}
