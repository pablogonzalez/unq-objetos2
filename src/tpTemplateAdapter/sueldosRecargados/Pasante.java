package sueldosRecargados;



/**
 * A	los	Pasantes	se	les	paga	$40	las	horas	trabajadas	en	el	
 * mes.
 * @author PabloGabriel
 *
 */
public class Pasante extends Empleado {

	private Float valorHora;
	private Integer horasTrabajadas;
	
	public Pasante(Integer horasTrabajadas) {
		setValorHora(40F);
		setHorasTrabajadas(horasTrabajadas);
	}
	
	@Override
	protected Float importeAsignacionesFamiliares() {
		return 0F;
	}

	@Override
	protected Float importeHorasTrabajadas() {
		return getValorHora() * getHorasTrabajadas();
	}

	@Override
	protected Float importeSueldoBasico() {
		return 0F;
	}

	public Float getValorHora() {
		return valorHora;
	}

	public void setValorHora(Float valorHora) {
		this.valorHora = valorHora;
	}

	public Integer getHorasTrabajadas() {
		return horasTrabajadas;
	}

	public void setHorasTrabajadas(Integer horasTrabajadas) {
		this.horasTrabajadas = horasTrabajadas;
	}

}
