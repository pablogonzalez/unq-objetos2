package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import elementosSimilares.LinkEnComun;
import elementosSimilares.MismaLetraInicial;
import elementosSimilares.PropiedadEnComun;
import elementosSimilares.WikipediaPage;

class ElementosSimilaresTestCase {

	private LinkEnComun filtroLinkEnComun;
	private MismaLetraInicial filtroMismaLetraInicial;
	private PropiedadEnComun filtroPropiedadEnComun;
	private WikipediaPage page1;
	private WikipediaPage page2;
	private WikipediaPage page3;
	private WikipediaPage page4;
	private List<WikipediaPage> wikipedia;
	@BeforeEach
	void setUp() throws Exception {
		filtroLinkEnComun = new LinkEnComun();
		filtroMismaLetraInicial = new MismaLetraInicial();
		filtroPropiedadEnComun = new PropiedadEnComun();
		page1 = new WikipediaPage("Bernal", new ArrayList<WikipediaPage>(), new HashMap<String, WikipediaPage>());
		page2 = new WikipediaPage("Buenos Aires", List.of(page1), hashOf( List.of("page1"), List.of(page1)));
		page3 = new WikipediaPage("Quilmes", List.of(page1, page2), hashOf( List.of("page1", "page2"), List.of(page1, page2)));
		page4 = new WikipediaPage("Avellaneda", List.of(page1, page2, page3), hashOf( List.of("page2", "page3"), List.of(page2, page3)));
		wikipedia = List.of(page1, page2, page3, page4);	
	}

	private HashMap<String, WikipediaPage> hashOf(List<String> keys, List<WikipediaPage> pages){
		HashMap<String, WikipediaPage> hash = new HashMap<String, WikipediaPage>();
		int index = 0;
		for (String key: keys) {
			hash.put(key, pages.get(index));
		    index++;
		}
		return hash;
	}
	
	@Test
	void testLinkEnComun() {
		assertEquals(3, filtroLinkEnComun.getSimilarPages(page2, wikipedia).size());
		assertEquals(0, filtroLinkEnComun.getSimilarPages(page1, wikipedia).size());
	}
	
	@Test
	void testMismaLetraInicial() {
		assertEquals(1, filtroMismaLetraInicial.getSimilarPages(page3, wikipedia).size());
		assertEquals(2, filtroMismaLetraInicial.getSimilarPages(page1, wikipedia).size());
	}

	@Test
	void testPropiedadEnComun() {
		assertEquals(3, filtroPropiedadEnComun.getSimilarPages(page3, wikipedia).size());
		assertEquals(2, filtroPropiedadEnComun.getSimilarPages(page2, wikipedia).size());
	}

}
