package ayudandoAlSoberano;

import java.util.ArrayList;
import java.util.List;

public abstract class CuentaBancaria {
	private String titular;
	private int saldo;
	private List<String> movimientos;
	
	public CuentaBancaria(String titular){
		this.titular=titular;
		this.saldo=0;
		this.movimientos = new ArrayList<String>();
	}
	public String getTitular(){
		return this.titular;
	}

	public int getSaldo(){
		return this.saldo;
	}
	
	public int getDescubierto() {
		return 0;
	}
	public int getLimite() {
		return getSaldo();
	}
	
	protected void setSaldo(int monto){
		this.saldo=monto;
	}
	
	public void agregarMovimientos(String movimiento){
		this.movimientos.add(movimiento);
	}
	
	public final int getSaldoExtraccion() {
		return this.getSaldo() + this.getDescubierto();
	}

	public final int getLimiteExtraccion() {
		return this.getLimite();
	}
	
	public final boolean validarExtraccion(int monto) {
		return getSaldoExtraccion() >= monto && getLimiteExtraccion() <= monto;
	}
	
	public final void extraer(int monto) {
		int saldoExtraccion = getSaldoExtraccion();
		
		if( validarExtraccion(monto) ){
			this.setSaldo(this.getSaldo()-monto);
			this.agregarMovimientos("Extraccion");
		}

	};

}